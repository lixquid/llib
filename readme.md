# Lua Library (LLib)

Hi! Welcome to the documentation for Lua Library!

------------------------------------------------------------------------------------------

## Initial Settings

LLib defines a global table, `llib`. By default, everything is accessable from, and only
from, this table. At the start of the file, some editable settings are available:

### `llib.version`

This value sets the current revision of LLib. It is strictly numerican and increments
every release, and so can be used to check for outdated versions.

### `llib.invasive`

This value determined the namespace for all LLib functions.
Valid values are:

`false` (default)
:	This defines `llib` as the namespace. This is the safest option, as the only
	possible conflicts are those that arise from other versions of LLib.

`true`
:	This defines the global table (`_G`) as the namespace. With this option set, all
	functions will be appended to existing tables. Should these tables not exist, LLib
	will create them.
	
	Take note! It's easy to overwrite functions if other libraries are using similar
	names!

------------------------------------------------------------------------------------------

## Assert Functions

Assert Functions can be used to check the validity of datatypes. 

Something to note is that all assert functions will return the checked object itself
if it passes validation; this can be used to simultaneously validate and set a default.

### `assertNumber`

