--[[--------------------------------------------------------------------------------------
	LuaLibrary - r11
	
	A library of Lua functions
--]]--------------------------------------------------------------------------------------

llib = {}
llib.version = 11
llib.invasive = true

local ns = llib.invasive and _G or llib

--[[--------------------------------------------------------------------------------------
	Encapsulation
--]]--------------------------------------------------------------------------------------

local tonumber = tonumber
local tostring = tostring
local type = type
local stringLower = string.lower
local pairs = pairs
local mathMax = math.max
local mathMin = math.min
local stringGsub = string.gsub
local mathLog = math.log
local mathFloor = math.floor
local stringSub = string.sub
local stringFormat = string.format
local mathHuge = math.huge
local stringUpper = string.upper
local stringFind = string.find
local setmetatable = setmetatable
local getmetatable = getmetatable
local stringRep = string.rep
local mathRandom = math.random
local tableSort = table.sort

--[[--------------------------------------------------------------------------------------
	Assert
--]]--------------------------------------------------------------------------------------

--- Assert that an object is a number.
-- All assert functions attempt to convert the passed object to the intended form. If the
-- object cannot be converted, nil is returned.
-- @param object The object to be asserted.
-- @return Object converted to number, nil otherwise.
function ns.assertNumber( object )
	return tonumber( object )
end

--- Assert that an object is a string.
-- All assert functions attempt to convert the passed object to the intended form. If the
-- object cannot be converted, nil is returned.
-- @param object The object to be asserted.
-- @return Object converted to string, nil otherwise.
function ns.assertString( object )
	return object ~= nil and tostring( object ) or nil
end

--- Assert that an object is a table.
-- The table assert function simply checks if the object is a table.
-- @param object The object to be asserted.
-- @return Object if a table, nil otherwise
function ns.assertTable( object )
	return type( object ) == "table" and object or nil
end

--[[--------------------------------------------------------------------------------------
	General
--]]--------------------------------------------------------------------------------------

--- Convert many "false" values to boolean false.
-- Convert false values such as numeric 0 and the empty string into boolean false.
-- In boolean logic, Lua typically treats these items as boolean true.
-- <br />Examples:<br /><code>
-- > 0
-- false
-- > ""
-- false
-- > "0"
-- false
-- > {}
-- false</code>
-- @param object The object to be checked.
-- @return Truth status of object.
function ns.toBool( object )
	if not object then
		return false
	elseif type( object ) == "number" then
		return object ~= 0
	elseif type( object ) == "string" then
		return object ~= "" and object ~= "0" and stringLower( object ) ~= "false"
	elseif type( object ) == "table" then
		for _ in pairs( object ) do return true end
		return false
	end
	return true
end

--[[--------------------------------------------------------------------------------------
	Math
--]]--------------------------------------------------------------------------------------
ns.math = ns.math or {}
local nsl = ns.math

--- Clamps a number between two values.
-- @param number Number to be clamped.
-- @param minimum Lower bound of number.
-- @param maximum Upper bound of number.
-- @return Number clamped between two values.
function nsl.clamp( number, minimum, maximum )
	return mathMax( mathMin( number, maximum ), minimum )
end

--- Comma-Delimits a number.
-- Converts a number into a string, and inserts commas at the correct points.
-- <br />Examples:<br /><code>
-- > 1234
-- "1,234"
-- > -1234567890.123
-- "-1,234,567,890.123"</code>
-- @param number Number to be Delimited.
-- @param delimiter (Optional) Alternate delimiter. Defaults to ",".
function nsl.comma( number, delimiter )
	local number, delimiter, hit = tostring( number ), delimiter or ","
	while true do
		number, hit = stringGsub( number, "^(-?%d+)(%d%d%d)", "%1" .. delimiter .. "%2" )
		if hit == 0 then return number end
	end
end

--- Linearly interpolates one number to another.
-- <br />Examples:<br /><code>
-- > 5, 10, 0.5 
-- 7.5</code>
-- @param number Starting number.
-- @param target Target number.
-- @param amount Factor to linearly interpolate by.
-- @return Number linearly interpolated.
function nsl.lerp( number, target, amount )
	return number + ( target - number ) * amount
end

--- Return the log of a number to any base.
-- @param number Number to take the log of.
-- @param base Base to take the log with.
-- @return Number taken to log.
function nsl.logB( number, base )
	return mathLog( number ) / mathLog( base )
end

local metrictable = { { "k", "kilo" }, { "M", "mega" }, { "G", "giga" },
	{ "T", "tera" }, { "P", "peta" }, { "E", "exa" }, { "Z", "zetta" },
	{ "Y", "yotta" }, [0] = { "", "" }, [-1] = { "m", "milli" },
	[-2] = { "u", "micro" }, [-3] = { "n", "nano" }, [-4] = { "p", "pico" },
	[-5] = { "f", "femto" }, [-6] = { "a", "atto" }, [-7] = { "z", "zepto" },
	[-8] = { "y", "yocto" } }
--- Return a metricized number and relevent suffixes.
-- <br />Examples:<br /><code>
-- > 5000
-- 5, "K", "kilo"
-- > 1230000
-- 1.23, "M", "mega",
-- > 0.0000000009
-- 900, "p", "pico"</code>
-- @param number Number to be metricized
-- @param base (Optional) Base of the metric system. Defaults to 1000.
-- @return Number reduced by metric amount
-- @return Correct Suffix
-- @return Full metric prefix name
function nsl.metric( number, base )
	local base = base or 1000
	
	local div = mathMax( mathMin( mathFloor(
		mathLog( number ) / mathLog( base ) ), 8 ), -8 )
	return number / base ^ div, unpack( metrictable[ div ] )
end

--- Return the ordinal suffix of a number.
-- <br />Examples:<br /><code>
-- > 1
-- "st"
-- > 3
-- "rd"
-- > 13
-- "th"</code>
-- @param number Number to return the ordinal suffix of.
-- @return Ordinal suffix of number.
function nsl.ordinal( number )
	local last = stringSub( tostring( mathFloor( tonumber( number ) + 0.5 ) ), -1 )
	if tonumber( number ) > 10 and
		stringSub( tostring( mathFloor( number + 0.5 ) ), -2 ) == "1" then
		return "th"
	elseif last == "1" then
		return "st"
	elseif last == "2" then
		return "nd"
	elseif last == "3" then
		return "rd"
	end
	return "th"
end

--- Rounds a number, optionally to a specified amount of decimal points.
-- Take note that numbers always rounded twoards positive infinity. This means that
-- -6.5 will become -6, not -7 as might be expected.
-- <br />Examples:<br /><code>
-- > 5.5
-- 6
-- > 7.4
-- 7
-- > 6.54, 1
-- 6.5
-- > -6.5
-- -6</code>
-- @param number Number to be rounded.
-- @param dp (Optional) Amount of decimal points to round to. Defaults to 0.
function nsl.round( number, dp )
	return dp and mathFloor( number * 10 ^ dp + 0.5 ) / 10 ^ dp
		or mathFloor( number + 0.5 )
end

-- Binary Functions ----------------------------------------------------------------------

--- Converts a binary string into an integer.
-- <br />Examples:<br /><code>
-- > "1010101"
-- 85</code>
-- @param string Binary string to be converted.
-- @return Input string in base 10.
function nsl.binaryToInteger( string )
	return tonumber( string, 2 )
end

local bintable = { "001", "010", "011", "100", "101", "110", "111", [0] = "000" }
--- Converts an integer into a binary string.
-- Take note that negative numbers will be wrapped around the 64-bit mark.
-- <br />Examples:<br /><code>
-- > 5
-- "101"
-- > 11
-- "1011"
-- > -100
-- "11111111111111111111110000011000"</code>
-- @param number Number to be converted.
-- @return Number as a Binary String.
function nsl.integerToBinary( number )
	return ( stringGsub( stringGsub( stringFormat( "%o", number ), "(.)", function( c )
			return bintable[ tonumber( c ) ]
		end ), "^0*", "" ) )
end

-- Indeterminism Functions ---------------------------------------------------------------

--- Indeterminate.
nsl.ind = 0/0
--- Not a Number
nsl.nan = -nsl.ind

--- Checks if a number is not infinite, in either direction.
-- @param number Number to be checked.
-- @return Boolean stating if number is finite.
function nsl.isFinite( number )
	return -mathHuge < number and number < mathHuge
end

--- Checks if a number is infinite, and also returns the sign if it is.
-- @param number Number to be checked.
-- @return 1 if positive infinite, -1 if negative infinite, false otherwise.
function nsl.isInfinite( number )
	return mathHuge == number and 1 or -mathHuge == number and -1
end

--- Checks if an object is Not a Number
-- <br />Examples:<br /><code>
-- > 5
-- false
-- > 0/0
-- true</code>
-- @param object Object to be checked.
-- @return Boolean stating if an object is Not a Number.
function nsl.isNaN( object )
	return object ~= object
end

--[[--------------------------------------------------------------------------------------
	String
--]]--------------------------------------------------------------------------------------
ns.string = ns.string or {}
local nsl = ns.string

--- Converts a string into camelCase, optionally using a delimiter.
-- <br />Examples:<br /><code>
-- > "have a nice day"
-- "haveANiceDay"
-- > "very_long_filename", "_"
-- "veryLongFilename"</code>
-- @param string String to be converted.
-- @param delimiter (Optional) Alternate Delimiter.
--   Defaults to "%s" (Whitespace Character).
-- @return String converted into Camel Case.
function nsl.camelize( string, delimiter )
	local string, delimiter = tostring( string ), tostring( delimiter or "%s" )
	return ( stringGsub( stringLower( string ),
		"[" .. delimiter .. "]+(.)",
		stringUpper
	) )
end

--- Returns an iterator function that outputs each character in a string.
-- <br />Examples:<br /><code>
-- > for k, v in string.chars( "hello" ) do print( v ) end
-- h
-- e
-- l
-- l
-- o</code>
-- @param string String to extract characters from
-- @return Iterator function to iterate over the string's characters
function nsl.chars( string )
	return function( t )
		t[1] = t[1] + 1
		local c = stringSub( t[2], t[1], t[1] )
		if c and #c > 0 then return t[1], c end
	end, { 0, tostring( string ) }
end

--- Returns a number of characters from the start of a string
-- <br />Examples:<br /><code>
-- > "hello world", 5
-- "hello"</code>
-- @param string String to sample characters from
-- @param length Amount of characters to sample
-- @return A string of length <code>length</code> from the start of the string.
function nsl.left( string, length )
	return stringSub( tostring( string ), 1, length )
end

--- Returns a number of characters from the end of a string
-- <br />Examples:<br /><code>
-- > "hello world", 5
-- "world"</code>
-- @param string String to sample characters from
-- @param length Amount of characters to sample
-- @return A string of length <code>length</code> from the end of the string.
function nsl.right( string, length )
	return stringSub( string, -length )
end

--- Splits a string into a table using a delimiter.
-- Delimiter can be a Lua pattern. Optionally, if no delimiter is provided, a table
-- composed of each character in the string will be returned.
-- <br />Examples:<br /><code>
-- > "hello there world", "%s"
-- {"hello", "there", "world"}
-- > "hello"
-- {"h", "e", "l", "l", "o"}
-- > "this  is   a    sentence", "%s+"
-- {"this", "is", "a", "sentence"}
-- > "this  is   a    sentence", "%s+", true
-- {"this  is   a    sentence"}</code>
-- @param string String to be split
-- @param seperator (Optional) Delimiter. Can be a Lua pattern. Defaults to "".
-- @param plain (Optional) Sets <code>seperator</code> to be interpreted as plain text,
--   not a Lua pattern. Defaults to false.
-- @return A table consisting of string, delimited by seperator. The delimiters are not
--   included.
function nsl.split( string, seperator, plain )
	local string, seperator, t = tostring( string ), tostring( seperator or "" ), {}
	while true do
		local p1, p2 = stringFind( string, seperator, 1, plain )
		if not p1 or #string == 0 then
			if #string > 0 then t[#t+1] = string end
			return t
		end
		t[#t+1] = stringSub( string, 1, mathMax( p1, 2 ) - 1 )
		string = stringSub( string, mathMax( p2, 1 ) + 1 )
	end
end

-- Strip Functions -----------------------------------------------------------------------

--- Strips character(s) from both ends of a string.
-- <br />Examples:<br /><code>
-- > "  hello  "
-- "hello"
-- > "abc123cba", "ab"
-- "c123c"</code>
-- @param string String to strip characters from.
-- @param character (Optional) Character(s) to be stripped. Multiple characters can be
--   defined. Accepts Lua pattern character classes. Defaults to "%s".
function nsl.strip( string, character )
	return ( stringGsub( tostring( string ),
		"^[" .. tostring( character or "%s" ) .. "]*(.-)["
		.. tostring( character or "%s" ) .. "]*$", "%1" ) )
end

--- Strips character(s) from the start of a string.
-- <br />Examples:<br /><code>
-- > "  hello  "
-- "hello  "
-- > "abc123cba", "ab"
-- "c123cba"</code>
-- @param string String to strip characters from.
-- @param character (Optional) Character(s) to be stripped. Multiple characters can be
--   defined. Accepts Lua pattern character classes. Defaults to "%s".
function nsl.stripLeft( string, character )
	return ( stringGsub( tostring( string ), "^[" .. tostring( character or "%s" )
		.. "]*", "" ) )
end

--- Strips character(s) from the end of a string.
-- <br />Examples:<br /><code>
-- > "  hello  "
-- "  hello"
-- > "abc123cba", "ab"
-- "abc123c"</code>
-- @param string String to strip characters from.
-- @param character (Optional) Character(s) to be stripped. Multiple characters can be
--   defined. Accepts Lua pattern character classes. Defaults to "%s".
function nsl.stripRight( string, character )
	return ( stringGsub( tostring( string ), "[" .. tostring( character or "%s" )
		.. "]*$", "" ) )
end

--[[--------------------------------------------------------------------------------------
	Table
--]]--------------------------------------------------------------------------------------
ns.table = ns.table or {}
local nsl = ns.table

--- Checks for the existance of an element in a table.
-- <br />Examples:<br /><code>
-- > {1, 2, 3}, 3
-- true
-- > {1, 2, 3}, 4
-- false</code>
-- @param table Table to be checked.
-- @param object Object to check for.
-- @return Boolean stating if table contains object.
function nsl.contains( table, object )
	for _, v in pairs( table ) do if v == object then return true end end
	return false
end

--- Copies the contents of a table to a new table in memory.
-- Note that unless the recursive flag is set, tables contained inside the parent table
-- will still have the same address in memory. The new table will also have the same
-- metattable.
-- <br />Examples:<br /><code>
-- > {1, 2, 3}
-- {1, 2, 3}
-- > a = {1, 2, 3}
-- > {1, 2, a}
-- {1, 2, a}
-- > {1, 2, a}, true
-- {1, 2, {1, 2, 3}}</code>
-- @param table Parent table to be copied
-- @param recursive (Optional) Boolean. If true, will attempt to copy any sub-tables in
--   the parent table. Be aware that a loop will cause the function to crash!
--   Defaults to false.
-- @return A new table in memory with the contents of the parent, and the same metatable.
function nsl.copy( table, recursive )
	local new = {}
	
	for k, v in pairs( table ) do
		new[ k ] = type( v ) == "table" and recursive and nsl.copy( v ) or v
	end
	setmetatable( new, getmetatable( table ) )
	
	return new
end

--- Counts the number of elements in a table.
-- <br />Examples:<br /><code>
-- > {1, 2, 3}
-- 3
-- > {a = 1, b = 2, c = 3}
-- 3</code>
function nsl.count( table )
	local n = 0
	for _ in pairs( table ) do n = n + 1 end
	return n
end

function nsl.empty( table )
	for k in pairs( table ) do table[k] = nil end
	return table
end

function nsl.head( table )
	for k, v in pairs( table ) do return k, v end
end

function nsl.last( table )
	local lk, lv
	for k, v in pairs( table ) do lk, lv = k, v end
	return lk, lv
end

function nsl.merge( target, ... )
	for _, table in pairs( { ... } ) do
		for k, v in pairs( table ) do
			target[k] = v
		end
	end
	return target
end

function nsl.print( table, depth, recurse )
	depth, recurse = depth or 0, recurse or {}
	
	if depth == 0 then
		print( tostring( table ) .. " {" )
	end
	
	depth = depth + 1
	recurse[ tostring( table ) ] = true
	
	for k, v in pairs( table ) do
		if type( v ) == "table" and not recurse[ tostring( v ) ] then
			print( stringRep( "\t", depth )
				.. tostring( k ) .. ": " .. tostring( v ) .. " {" )
			nsl.print( v, depth, recurse )
		else
			print( stringRep( "\t", depth ) .. tostring( k ) .. ": " .. tostring( v ) )
		end
	end
	
	depth = depth - 1
	print( stringRep( "\t", depth ) .. "}" )
end

function nsl.random( table )
	local t = {}
	for _, v in pairs( table ) do t[#t+1] = v end
	return t[ math.random( #t ) ]
end

-- Mapping Functions ---------------------------------------------------------------------

function nsl.exists( table, func )
	for _, v in pairs( table ) do if func( v ) then return true end end
	return false
end

function nsl.forall( table, func )
	for _, v in pairs( table ) do if not func( v ) then return false end end
	return true
end

function nsl.map( table, func )
	for k, v in pairs( table ) do table[k] = func( v ) end
	return table
end

-- Sorting Functions----------------------------------------------------------------------

function nsl.sortAscending( table )
	return tableSort( table )
end

function nsl.sortDescending( table )
	return tableSort( table, function( a, b ) return b < a end )
end

-- Set Functions -------------------------------------------------------------------------

nsl.union = nsl.merge

function nsl.isSubset( table, super )
	for k in pairs( table ) do if super[k] == nil then return false end end
	return true
end

function nsl.difference( target, ... )
	local new = {}
	for k, v in pairs( target ) do
		new[k] = v
		for _, table in pairs( {...} ) do
			if table[k] then new[k] = nil break end
		end
	end
	return new
end

function nsl.intersection( first, ... )
	local count = {}
	local new = {}
	
	for k, v in pairs( first ) do
		count[k] = 1
	end
	
	for _, table in pairs( {...} ) do
		for k in pairs( table ) do
			count[k] = count[k] and count[k] + 1
		end
	end
	
	local n = select( "#", ... ) + 1
	for k, v in pairs( count ) do
		if v == n then
			new[k] = first[k]
		end
	end
	
	return new
end

--[[--------------------------------------------------------------------------------------
	Garry's Mod Drawing
--]]--------------------------------------------------------------------------------------

if gmod and CLIENT then

-- Encapsulation -------------------------------------------------------------------------
 
local surfaceSetDrawColor = surface.SetDrawColor
local surfaceDrawRect = surface.DrawRect
local surfaceDrawOutlinedRect = surface.DrawOutlinedRect
local Material = Material
local surfaceSetMaterial = surface.SetMaterial
local surfaceDrawTexturedRect = surface.DrawTexturedRect
local surfaceDrawTexturedRectRotated = surface.DrawTexturedRectRotated
local surfaceGetTextureID = surface.GetTextureID
local surfaceSetTexture = surface.SetTexture
local surfaceSetTextColor = surface.SetTextColor
local surfaceSetFont = surface.SetFont
local surfaceGetTextSize = surface.GetTextSize
local surfaceSetTextPos = surface.SetTextPos
local surfaceDrawText = surface.DrawText

ns.draw = ns.draw or {}
nsl = ns.draw

-- Basic Rendering -----------------------------------------------------------------------

function nsl.rect( x, y, w, h, color )
	if color then surfaceSetDrawColor( color ) end
	surfaceDrawRect( mathFloor( x ), mathFloor( y ), mathFloor( w ), mathFloor( h ) )
end

function nsl.rectOutlined( x, y, w, h, color )
	if color then surfaceSetDrawColor( color ) end
	surfaceDrawOutlinedRect( mathFloor( x ), mathFloor( y ), mathFloor( w ),
		mathFloor( h ) )
end

local mat_cache = {}
function nsl.rectTextured( x, y, w, h, color, material, override )
	if type( material ) == "string" and not mat_cache[ material ] or override then
		mat_cache[ material ] = Material( material )
	end
	if color then surfaceSetDrawColor( color ) end
	surfaceSetMaterial( mat_cache[ material ] or material )
	surfaceDrawTexturedRect( mathFloor( x ), mathFloor( y ), mathFloor( w ),
		mathFloor( h ) )
end

function nsl.rectTexturedRotated( x, y, w, h, color, material, rotation, override )
	if type( material ) == "string" and not mat_cache[ material ] or override then
		mat_cache[ material ] = Material( material )
	end
	if color then surfaceSetDrawColor( color ) end
	surfaceSetMaterial( mat_cache[ material ] or material )
	surfaceDrawTexturedRectRotated( mathFloor( x ), mathFloor( y ), mathFloor( w ),
		mathFloor( h ), rotation )
end

local mat_corner8 = surfaceGetTextureID( "gui/corner8" )
local mat_corner16 = surfaceGetTextureID( "gui/corner16" )

function nsl.rectRounded( x, y, w, h, color, radius, tl, tr, bl, br )
	local radius = mathFloor( radius )
	if color then surfaceSetDrawColor( color ) end
	surfaceSetTexture( radius > 8 and mat_corner16 or mat_corner8 )
	
	surfaceDrawRect( x + radius, y, w - radius * 2, radius )
	surfaceDrawRect( x + radius, y + h - radius, w - radius * 2, radius )
	surfaceDrawRect( x, y + radius, w, h - radius * 2 )
	
	if tl == nil or tl then
		surfaceDrawTexturedRectRotated( x + radius / 2, y + radius / 2, radius, radius,
			0 )
	else
		surfaceDrawRect( x, y, radius, radius )
	end
	if tr == nil or tr then
		surfaceDrawTexturedRectRotated( x + w - radius / 2, y + radius / 2, radius,
		radius, 270 )
	else
		surfaceDrawRect( x + w - radius, y, radius, radius )
	end
	if bl == nil or bl then
		surfaceDrawTexturedRectRotated( x + radius / 2, y + h - radius / 2, radius,
			radius, 90 )
	else
		surfaceDrawRect( x, y + h - radius, radius, radius )
	end
	if br == nil or br then
		surfaceDrawTexturedRectRotated( x + w - radius / 2, y + h - radius / 2, radius,
		radius, 180 )
	else
		surfaceDrawRect( x + w - radius, y + h - radius, radius, radius )
	end
end

-- Text Rendering ------------------------------------------------------------------------

function nsl.text( text, font, x, y, color, ax, ay )
	if color then surfaceSetTextColor( color ) end
	surfaceSetFont( font )
	local w, h = surfaceGetTextSize( text )
	surfaceSetTextPos(
		mathFloor( x - w * ( ax or 0 ) ),
		mathFloor( y - h * ( ay or 0 ) )
	)
	surfaceDrawText( text )
	return w, h
end

function nsl.textShadowed( text, font, x, y, color, ax, ay, sd, scolor )
	surfaceSetFont( font )
	local w, h = surfaceGetTextSize( text )
	local x, y = mathFloor( x - w * ( ax or 0 ) ), mathFloor( y - h * ( ay or 0 ) )
	
	if scolor then surfaceSetTextColor( scolor ) end
	surfaceSetTextPos( x + sd, y + sd )
	surfaceDrawText( text )
	
	if color then surfaceSetTextColor( color ) end
	surfaceSetTextPos( x, y )
	surfaceDrawText( text )
	return w, h
end

function nsl.textOutlined( text, font, x, y, color, ax, ay, outline, ocolor )
	surfaceSetFont( font )
	local w, h = surfaceGetTextSize( text )
	local x, y = mathFloor( x - w * ( ax or 0 ) ), mathFloor( y - h * ( ay or 0 ) )
	
	if ocolor then surfaceSetTextColor( ocolor ) end
	for i = -outline, outline do
		for j = -outline, outline do
			surfaceSetTextPos( x + i, y + j )
			surfaceDrawText( text )
		end
	end
	
	if color then surfaceSetTextColor( color ) end
	surfaceSetTextPos( x, y )
	surfaceDrawText( text )
	return w, h
end

-- Utility -------------------------------------------------------------------------------

function nsl.materialCache( material )
	return mat_cache[ material ]
end


end
